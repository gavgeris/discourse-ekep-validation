# frozen_string_literal: true

# name: discourse-ekep-validation
# about: TODO
# version: 0.0.1
# authors: Discourse
# url: TODO
# required_version: 2.7.0

enabled_site_setting :discourse-ekep-validation_enabled

after_initialize {}

require_dependency 'user'

validate User, :kep_validation_method do
  field = self.custom_fields['kep']
  if ! field.blank? && !field.include?("kep1")
    self.errors[:cool_tapes] << "Not valid KEP Number"
  end
end